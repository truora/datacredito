package main

import (
	"bitbucket.org/truora/scrap-services/logger"
	"bitbucket.org/truora/scrap-services/shared"
	"bitbucket.org/truora/scrap-services/shared/client"
	"bytes"
	"context"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"encoding/xml"
	"errors"
	"fmt"
	"github.com/ucarion/c14n"
	"net/http"
	"strings"
	"text/template"
)

var (
	// ErrMissingDocNumber document number not found
	ErrMissingDocNumber = errors.New("missing document number")
	// ErrMissingLogger error when missing logger
	ErrMissingLogger = errors.New("missing logger")
	// ErrMissingClient error when missing client
	ErrMissingClient = errors.New("missing client")

	certPublic = `-----BEGIN CERTIFICATE-----
MIIGLTCCBRWgAwIBAgIQaeukNcIaCZXLEJvbPjoo6jANBgkqhkiG9w0BAQsFADCB
jzELMAkGA1UEBhMCR0IxGzAZBgNVBAgTEkdyZWF0ZXIgTWFuY2hlc3RlcjEQMA4G
A1UEBxMHU2FsZm9yZDEYMBYGA1UEChMPU2VjdGlnbyBMaW1pdGVkMTcwNQYDVQQD
Ey5TZWN0aWdvIFJTQSBEb21haW4gVmFsaWRhdGlvbiBTZWN1cmUgU2VydmVyIENB
MB4XDTIxMDcxOTAwMDAwMFoXDTIyMDgxOTIzNTk1OVowFTETMBEGA1UEAxMKdHJ1
b3JhLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAM39InS8hZEi
p5WJIGQgfGPN9uFClMZI/aAjI1rhdaD5OJH8f6GKkjzzGHb7mK2qGTv1v+LOqi6L
GJGQ58RxdvaA8jtSHIHR9bvucbO0bqHcGWiBCZ3rUoMKYDjK245JtwN5RHzd3R34
QKQr6v7Q6AVhLioT3PiTbUu8jSNkM5lIvbannnBoT2FKnN4vLp1FEB6aH95/x3+2
OgGGsyTPfqhRD8vzNm4uA7ZNn5JOE6YcOxD5uFrMMhghjcKWZvKmqd9sX4DsNmh/
Nry4+kbSxmLRkJ62k7RfvNd0hjOX0pJrkmVEMhDJBMOYuPEPNvZmaad4g0aJRmS9
JA8SgIYjHj8CAwEAAaOCAvwwggL4MB8GA1UdIwQYMBaAFI2MXsRUrYrhd+mb+ZsF
4bgBjWHhMB0GA1UdDgQWBBTQjAzahel0nUcR48qMClPu+uItTzAOBgNVHQ8BAf8E
BAMCBaAwDAYDVR0TAQH/BAIwADAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUH
AwIwSQYDVR0gBEIwQDA0BgsrBgEEAbIxAQICBzAlMCMGCCsGAQUFBwIBFhdodHRw
czovL3NlY3RpZ28uY29tL0NQUzAIBgZngQwBAgEwgYQGCCsGAQUFBwEBBHgwdjBP
BggrBgEFBQcwAoZDaHR0cDovL2NydC5zZWN0aWdvLmNvbS9TZWN0aWdvUlNBRG9t
YWluVmFsaWRhdGlvblNlY3VyZVNlcnZlckNBLmNydDAjBggrBgEFBQcwAYYXaHR0
cDovL29jc3Auc2VjdGlnby5jb20wJQYDVR0RBB4wHIIKdHJ1b3JhLmNvbYIOd3d3
LnRydW9yYS5jb20wggF+BgorBgEEAdZ5AgQCBIIBbgSCAWoBaAB2AEalVet1+pEg
MLWiiWn0830RLEF0vv1JuIWr8vxw/m1HAAABesDKoO4AAAQDAEcwRQIgS0JhLFjw
3A7xXX4nbdTqNvXUtIRmmgf15vm9N6HWgzMCIQDPo0wXBdH3E0oha06VrI44PcdS
AMCd9TRjMn1sRrwQXgB1AEHIyrHfIkZKEMahOglCh15OMYsbA+vrS8do8JBilgb2
AAABesDKoPYAAAQDAEYwRAIgUyEDYKA7LvbVO6K9R0C6iyK7DkDhXVhE+aPnpyRO
FYcCICqmaGeSdfVt+j5PcNthKTlvuWcH+q3GRbeDWkkjvR7oAHcAKXm+8J45OSHw
VnOfY6V35b5XfZxgCvj5TV0mXCVdx4QAAAF6wMqgzgAABAMASDBGAiEA0Vnv+kkP
vcV6pwu0E2+LvJULhLW79+bS2Tc463fM6wYCIQD9Cz4htCd4bIIVFbKw59uCCSYP
6gv/5/mCjWE+Hz0KLzANBgkqhkiG9w0BAQsFAAOCAQEAbTJP2tk3JYkea8a0GPue
XbwkOqBWLgp9vElCWQaitEtVl42Wsdm5GPc8/lZGOojep/rUZTCwZ3hR3Xag42R8
NpeFUboijlqSxvHlOJKTA68wIGWjGgNk7IA2IaL4awIea68XZAVeHqBH1D+05VNX
nm7AyoXJPtVDVTO5k0uC8XBpRTf0ZrykzqISbSov0gsX9/trJA56VNN1cwlg9EZJ
kaC/0oYE6Rpddw90FCHItBGWurG/kTMFZnMk9Z6Lg1j1kwrarpppkk8qRGDh80Jl
ZpxplCdi9rbK8DKGrMkb4m3bex6WTaowZhMwvjQWIrZJQAAzyN0kOFhPd3JQMfF5
gw==
-----END CERTIFICATE-----`
	certPublicClear = `MIIGLTCCBRWgAwIBAgIQaeukNcIaCZXLEJvbPjoo6jANBgkqhkiG9w0BAQsFADCBjzELMAkGA1UEBhMCR0IxGzAZBgNVBAgTEkdyZWF0ZXIgTWFuY2hlc3RlcjEQMA4GA1UEBxMHU2FsZm9yZDEYMBYGA1UEChMPU2VjdGlnbyBMaW1pdGVkMTcwNQYDVQQDEy5TZWN0aWdvIFJTQSBEb21haW4gVmFsaWRhdGlvbiBTZWN1cmUgU2VydmVyIENBMB4XDTIxMDcxOTAwMDAwMFoXDTIyMDgxOTIzNTk1OVowFTETMBEGA1UEAxMKdHJ1b3JhLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAM39InS8hZEip5WJIGQgfGPN9uFClMZI/aAjI1rhdaD5OJH8f6GKkjzzGHb7mK2qGTv1v+LOqi6LGJGQ58RxdvaA8jtSHIHR9bvucbO0bqHcGWiBCZ3rUoMKYDjK245JtwN5RHzd3R34QKQr6v7Q6AVhLioT3PiTbUu8jSNkM5lIvbannnBoT2FKnN4vLp1FEB6aH95/x3+2OgGGsyTPfqhRD8vzNm4uA7ZNn5JOE6YcOxD5uFrMMhghjcKWZvKmqd9sX4DsNmh/Nry4+kbSxmLRkJ62k7RfvNd0hjOX0pJrkmVEMhDJBMOYuPEPNvZmaad4g0aJRmS9JA8SgIYjHj8CAwEAAaOCAvwwggL4MB8GA1UdIwQYMBaAFI2MXsRUrYrhd+mb+ZsF4bgBjWHhMB0GA1UdDgQWBBTQjAzahel0nUcR48qMClPu+uItTzAOBgNVHQ8BAf8EBAMCBaAwDAYDVR0TAQH/BAIwADAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwSQYDVR0gBEIwQDA0BgsrBgEEAbIxAQICBzAlMCMGCCsGAQUFBwIBFhdodHRwczovL3NlY3RpZ28uY29tL0NQUzAIBgZngQwBAgEwgYQGCCsGAQUFBwEBBHgwdjBPBggrBgEFBQcwAoZDaHR0cDovL2NydC5zZWN0aWdvLmNvbS9TZWN0aWdvUlNBRG9tYWluVmFsaWRhdGlvblNlY3VyZVNlcnZlckNBLmNydDAjBggrBgEFBQcwAYYXaHR0cDovL29jc3Auc2VjdGlnby5jb20wJQYDVR0RBB4wHIIKdHJ1b3JhLmNvbYIOd3d3LnRydW9yYS5jb20wggF+BgorBgEEAdZ5AgQCBIIBbgSCAWoBaAB2AEalVet1+pEgMLWiiWn0830RLEF0vv1JuIWr8vxw/m1HAAABesDKoO4AAAQDAEcwRQIgS0JhLFjw3A7xXX4nbdTqNvXUtIRmmgf15vm9N6HWgzMCIQDPo0wXBdH3E0oha06VrI44PcdSAMCd9TRjMn1sRrwQXgB1AEHIyrHfIkZKEMahOglCh15OMYsbA+vrS8do8JBilgb2AAABesDKoPYAAAQDAEYwRAIgUyEDYKA7LvbVO6K9R0C6iyK7DkDhXVhE+aPnpyROFYcCICqmaGeSdfVt+j5PcNthKTlvuWcH+q3GRbeDWkkjvR7oAHcAKXm+8J45OSHwVnOfY6V35b5XfZxgCvj5TV0mXCVdx4QAAAF6wMqgzgAABAMASDBGAiEA0Vnv+kkPvcV6pwu0E2+LvJULhLW79+bS2Tc463fM6wYCIQD9Cz4htCd4bIIVFbKw59uCCSYP6gv/5/mCjWE+Hz0KLzANBgkqhkiG9w0BAQsFAAOCAQEAbTJP2tk3JYkea8a0GPueXbwkOqBWLgp9vElCWQaitEtVl42Wsdm5GPc8/lZGOojep/rUZTCwZ3hR3Xag42R8NpeFUboijlqSxvHlOJKTA68wIGWjGgNk7IA2IaL4awIea68XZAVeHqBH1D+05VNXnm7AyoXJPtVDVTO5k0uC8XBpRTf0ZrykzqISbSov0gsX9/trJA56VNN1cwlg9EZJkaC/0oYE6Rpddw90FCHItBGWurG/kTMFZnMk9Z6Lg1j1kwrarpppkk8qRGDh80JlZpxplCdi9rbK8DKGrMkb4m3bex6WTaowZhMwvjQWIrZJQAAzyN0kOFhPd3JQMfF5gw==`
	privateKey      = `` //TODO: add the Truora Private Key. Talk with Morán, Alegría or Ricardo Arias

	usernameOKTA = "2-901189979"
	passwordOKTA = "TruoraContraElMundo02!"
	key          = "14ELJ"
	product      = 64
	truoraUser   = 901189979
)

// Fetcher handler
type Fetcher struct {
	documentNumber string
	lastName       string
	documentType   int
	client         *client.Client
	logger         *logger.Logger
}

type Request struct {
	Header string
	Body   string
}

type RequestHeader struct {
	BinarySecurityTokenID    string
	TruoraCertificatePublic  string
	UsernameTokenID          string
	UsernameOKTA             string
	PasswordOKTA             string
	SignedInfo               string
	SignatureID              string
	SignatureValue           string
	KeyInfoID                string
	SecurityTokenReferenceID string
}

type SignedInfo struct {
	BodyID      string
	DigestValue string
}

type RequestBody struct {
	BodyID             string
	Key                string
	DocumentNumber     string
	FirstLastName      string
	Product            int
	IdentificationType int
	User               int
}

// NewFetcher returns a Fetcher instance
func NewFetcher(documentNumber, lastName string, documentType int, log *logger.Logger, cli *client.Client) (*Fetcher, error) {
	if documentNumber == "" {
		return nil, ErrMissingDocNumber
	}

	if cli == nil {
		return nil, ErrMissingClient
	}

	if log == nil {
		return nil, ErrMissingLogger
	}

	return &Fetcher{
		documentNumber: documentNumber,
		lastName:       lastName,
		documentType:   documentType,
		client:         cli,
		logger:         log,
	}, nil
}

// Execute collect the information
func (fetcher *Fetcher) Execute(ctx context.Context) error {
	bodyID := generateID("id")
	reqBody := &RequestBody{
		BodyID:             bodyID,
		Key:                key,
		DocumentNumber:     fetcher.documentNumber,
		FirstLastName:      fetcher.lastName,
		Product:            product,
		IdentificationType: fetcher.documentType,
		User:               truoraUser,
	}

	bodyXMLString, bodyXMLBytes, err := executeTemplateXML("./samples/request_body_template.xml", reqBody)
	if err != nil {
		return err
	}

	digestValue, err := getDigestValue(bodyXMLBytes)
	if err != nil {
		return err
	}

	signInfo := &SignedInfo{
		BodyID:      bodyID,
		DigestValue: digestValue,
	}

	signedInfoXMLString, signedInfoXMLBytes, err := executeTemplateXML("./samples/request_signed_info_template.xml", signInfo)
	if err != nil {
		return err
	}

	signValue, err := getSignatureValue(signedInfoXMLBytes)
	if err != nil {
		return err
	}

	reqHeader := &RequestHeader{
		BinarySecurityTokenID:    generateID("X509"),
		TruoraCertificatePublic:  certPublicClear,
		UsernameTokenID:          generateID("UsernameToken"),
		UsernameOKTA:             usernameOKTA,
		PasswordOKTA:             passwordOKTA,
		SignatureID:              generateID("SIG"),
		SignedInfo:               signedInfoXMLString,
		SignatureValue:           signValue,
		KeyInfoID:                generateID("KI"),
		SecurityTokenReferenceID: generateID("STR"),
	}

	headerXMLString, _, err := executeTemplateXML("./samples/request_header_template.xml", reqHeader)
	if err != nil {
		return err
	}

	req := &Request{
		Header: headerXMLString,
		Body:   bodyXMLString,
	}

	_, requestXMLBytes, err := executeTemplateXML("./samples/request_soap_template.xml", req)
	if err != nil {
		return err
	}

	canonicalizeRequest, err := canonicalizeC14N(requestXMLBytes)
	if err != nil {
		return err
	}

	cert, err := tls.X509KeyPair([]byte(certPublic), []byte(privateKey))
	if err != nil {
		return err
	}

	fetcher.client.HTTPClient.Transport = &http.Transport{
		TLSClientConfig: &tls.Config{
			Certificates: []tls.Certificate{
				cert,
			},
			InsecureSkipVerify: true,
		},
	}

	header := http.Header{}
	header.Set("Content-Type", "text/xml;charset=UTF-8")
	header.Set("SOAPAction", "")
	header.Set("Accept-Encoding", "gzip,deflate")

	response, err := fetcher.client.SendRequest(ctx, http.MethodPost, "https://demo-servicesesb.datacredito.com.co/wss/dhws3/services/DHServicePlus", header, string(canonicalizeRequest))
	if err != nil {
		return err
	}

	defer fetcher.logger.CloseOrLog(ctx, response.Body)

	buf := new(bytes.Buffer)

	_, err = buf.ReadFrom(response.Body)
	if err != nil {
		return err
	}

	newStr := buf.String()
	fmt.Printf(newStr)

	if response.StatusCode != http.StatusOK {
		return errors.New("http status different")
	}

	return nil
}

func generateID(prefix string) string {
	return prefix + "-" + strings.ToUpper(shared.GenerateRandomHexString(16))
}

func executeTemplateXML(file string, templateXML interface{}) (string, []byte, error) {
	tpl, err := template.ParseFiles(file)
	if err != nil {
		return "", nil, err
	}

	var templateOutput bytes.Buffer

	err = tpl.Execute(&templateOutput, templateXML)
	if err != nil {
		return "", nil, err
	}

	return templateOutput.String(), templateOutput.Bytes(), nil
}

func canonicalizeC14N(xmlBytes []byte) ([]byte, error) {
	decoder := xml.NewDecoder(bytes.NewReader(xmlBytes))

	out, err := c14n.Canonicalize(decoder)
	if err != nil {
		return nil, err
	}

	return out, nil
}

func getDigestValue(bodyBytes []byte) (string, error) {
	canonicalizeBody, err := canonicalizeC14N(bodyBytes)
	if err != nil {
		return "", err
	}

	h := sha1.New()
	h.Write(canonicalizeBody)

	return base64.StdEncoding.EncodeToString(h.Sum(nil)), nil
}

func getSignatureValue(signInfoBytes []byte) (string, error) {
	canonicalizeSignInfo, err := canonicalizeC14N(signInfoBytes)
	if err != nil {
		return "", err
	}

	h := sha1.New()
	h.Write(canonicalizeSignInfo)

	pem09, _ := pem.Decode([]byte(privateKey))

	privateKey09, err := x509.ParsePKCS1PrivateKey(pem09.Bytes)
	if err != nil {
		return "", err
	}

	sign, err := rsa.SignPKCS1v15(rand.Reader, privateKey09, crypto.SHA1, h.Sum(nil))
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(sign), nil
}
