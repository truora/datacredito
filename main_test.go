package main

import (
	"context"
	"testing"

	"bitbucket.org/truora/scrap-services/logger"
	"bitbucket.org/truora/scrap-services/shared/client"

	"github.com/stretchr/testify/require"
)

func TestFound(t *testing.T) {
	c := require.New(t)

	cli, err := client.New()
	c.NoError(err)

	fetcher, err := NewFetcher("1144194216", "MORAN", logger.New("test"), cli)
	c.NoError(err)

	err = fetcher.Execute(context.Background())
	c.NoError(err)
}
