package main

import (
	"bytes"
	"context"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/hex"
	"encoding/pem"
	"encoding/xml"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/truora/scrap-services/logger"
	"bitbucket.org/truora/scrap-services/shared/client"
)

const (
	_attemptsToReadRandomData = 3
	layoutTime                = "2006-01-02T15:04:05.999Z"
)

var (
	certPublic = `-----BEGIN CERTIFICATE-----
MIIGLTCCBRWgAwIBAgIQaeukNcIaCZXLEJvbPjoo6jANBgkqhkiG9w0BAQsFADCB
jzELMAkGA1UEBhMCR0IxGzAZBgNVBAgTEkdyZWF0ZXIgTWFuY2hlc3RlcjEQMA4G
A1UEBxMHU2FsZm9yZDEYMBYGA1UEChMPU2VjdGlnbyBMaW1pdGVkMTcwNQYDVQQD
Ey5TZWN0aWdvIFJTQSBEb21haW4gVmFsaWRhdGlvbiBTZWN1cmUgU2VydmVyIENB
MB4XDTIxMDcxOTAwMDAwMFoXDTIyMDgxOTIzNTk1OVowFTETMBEGA1UEAxMKdHJ1
b3JhLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAM39InS8hZEi
p5WJIGQgfGPN9uFClMZI/aAjI1rhdaD5OJH8f6GKkjzzGHb7mK2qGTv1v+LOqi6L
GJGQ58RxdvaA8jtSHIHR9bvucbO0bqHcGWiBCZ3rUoMKYDjK245JtwN5RHzd3R34
QKQr6v7Q6AVhLioT3PiTbUu8jSNkM5lIvbannnBoT2FKnN4vLp1FEB6aH95/x3+2
OgGGsyTPfqhRD8vzNm4uA7ZNn5JOE6YcOxD5uFrMMhghjcKWZvKmqd9sX4DsNmh/
Nry4+kbSxmLRkJ62k7RfvNd0hjOX0pJrkmVEMhDJBMOYuPEPNvZmaad4g0aJRmS9
JA8SgIYjHj8CAwEAAaOCAvwwggL4MB8GA1UdIwQYMBaAFI2MXsRUrYrhd+mb+ZsF
4bgBjWHhMB0GA1UdDgQWBBTQjAzahel0nUcR48qMClPu+uItTzAOBgNVHQ8BAf8E
BAMCBaAwDAYDVR0TAQH/BAIwADAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUH
AwIwSQYDVR0gBEIwQDA0BgsrBgEEAbIxAQICBzAlMCMGCCsGAQUFBwIBFhdodHRw
czovL3NlY3RpZ28uY29tL0NQUzAIBgZngQwBAgEwgYQGCCsGAQUFBwEBBHgwdjBP
BggrBgEFBQcwAoZDaHR0cDovL2NydC5zZWN0aWdvLmNvbS9TZWN0aWdvUlNBRG9t
YWluVmFsaWRhdGlvblNlY3VyZVNlcnZlckNBLmNydDAjBggrBgEFBQcwAYYXaHR0
cDovL29jc3Auc2VjdGlnby5jb20wJQYDVR0RBB4wHIIKdHJ1b3JhLmNvbYIOd3d3
LnRydW9yYS5jb20wggF+BgorBgEEAdZ5AgQCBIIBbgSCAWoBaAB2AEalVet1+pEg
MLWiiWn0830RLEF0vv1JuIWr8vxw/m1HAAABesDKoO4AAAQDAEcwRQIgS0JhLFjw
3A7xXX4nbdTqNvXUtIRmmgf15vm9N6HWgzMCIQDPo0wXBdH3E0oha06VrI44PcdS
AMCd9TRjMn1sRrwQXgB1AEHIyrHfIkZKEMahOglCh15OMYsbA+vrS8do8JBilgb2
AAABesDKoPYAAAQDAEYwRAIgUyEDYKA7LvbVO6K9R0C6iyK7DkDhXVhE+aPnpyRO
FYcCICqmaGeSdfVt+j5PcNthKTlvuWcH+q3GRbeDWkkjvR7oAHcAKXm+8J45OSHw
VnOfY6V35b5XfZxgCvj5TV0mXCVdx4QAAAF6wMqgzgAABAMASDBGAiEA0Vnv+kkP
vcV6pwu0E2+LvJULhLW79+bS2Tc463fM6wYCIQD9Cz4htCd4bIIVFbKw59uCCSYP
6gv/5/mCjWE+Hz0KLzANBgkqhkiG9w0BAQsFAAOCAQEAbTJP2tk3JYkea8a0GPue
XbwkOqBWLgp9vElCWQaitEtVl42Wsdm5GPc8/lZGOojep/rUZTCwZ3hR3Xag42R8
NpeFUboijlqSxvHlOJKTA68wIGWjGgNk7IA2IaL4awIea68XZAVeHqBH1D+05VNX
nm7AyoXJPtVDVTO5k0uC8XBpRTf0ZrykzqISbSov0gsX9/trJA56VNN1cwlg9EZJ
kaC/0oYE6Rpddw90FCHItBGWurG/kTMFZnMk9Z6Lg1j1kwrarpppkk8qRGDh80Jl
ZpxplCdi9rbK8DKGrMkb4m3bex6WTaowZhMwvjQWIrZJQAAzyN0kOFhPd3JQMfF5
gw==
-----END CERTIFICATE-----`

	certPublicClear = `MIIGLTCCBRWgAwIBAgIQaeukNcIaCZXLEJvbPjoo6jANBgkqhkiG9w0BAQsFADCBjzELMAkGA1UEBhMCR0IxGzAZBgNVBAgTEkdyZWF0ZXIgTWFuY2hlc3RlcjEQMA4GA1UEBxMHU2FsZm9yZDEYMBYGA1UEChMPU2VjdGlnbyBMaW1pdGVkMTcwNQYDVQQDEy5TZWN0aWdvIFJTQSBEb21haW4gVmFsaWRhdGlvbiBTZWN1cmUgU2VydmVyIENBMB4XDTIxMDcxOTAwMDAwMFoXDTIyMDgxOTIzNTk1OVowFTETMBEGA1UEAxMKdHJ1b3JhLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAM39InS8hZEip5WJIGQgfGPN9uFClMZI/aAjI1rhdaD5OJH8f6GKkjzzGHb7mK2qGTv1v+LOqi6LGJGQ58RxdvaA8jtSHIHR9bvucbO0bqHcGWiBCZ3rUoMKYDjK245JtwN5RHzd3R34QKQr6v7Q6AVhLioT3PiTbUu8jSNkM5lIvbannnBoT2FKnN4vLp1FEB6aH95/x3+2OgGGsyTPfqhRD8vzNm4uA7ZNn5JOE6YcOxD5uFrMMhghjcKWZvKmqd9sX4DsNmh/Nry4+kbSxmLRkJ62k7RfvNd0hjOX0pJrkmVEMhDJBMOYuPEPNvZmaad4g0aJRmS9JA8SgIYjHj8CAwEAAaOCAvwwggL4MB8GA1UdIwQYMBaAFI2MXsRUrYrhd+mb+ZsF4bgBjWHhMB0GA1UdDgQWBBTQjAzahel0nUcR48qMClPu+uItTzAOBgNVHQ8BAf8EBAMCBaAwDAYDVR0TAQH/BAIwADAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwSQYDVR0gBEIwQDA0BgsrBgEEAbIxAQICBzAlMCMGCCsGAQUFBwIBFhdodHRwczovL3NlY3RpZ28uY29tL0NQUzAIBgZngQwBAgEwgYQGCCsGAQUFBwEBBHgwdjBPBggrBgEFBQcwAoZDaHR0cDovL2NydC5zZWN0aWdvLmNvbS9TZWN0aWdvUlNBRG9tYWluVmFsaWRhdGlvblNlY3VyZVNlcnZlckNBLmNydDAjBggrBgEFBQcwAYYXaHR0cDovL29jc3Auc2VjdGlnby5jb20wJQYDVR0RBB4wHIIKdHJ1b3JhLmNvbYIOd3d3LnRydW9yYS5jb20wggF+BgorBgEEAdZ5AgQCBIIBbgSCAWoBaAB2AEalVet1+pEgMLWiiWn0830RLEF0vv1JuIWr8vxw/m1HAAABesDKoO4AAAQDAEcwRQIgS0JhLFjw3A7xXX4nbdTqNvXUtIRmmgf15vm9N6HWgzMCIQDPo0wXBdH3E0oha06VrI44PcdSAMCd9TRjMn1sRrwQXgB1AEHIyrHfIkZKEMahOglCh15OMYsbA+vrS8do8JBilgb2AAABesDKoPYAAAQDAEYwRAIgUyEDYKA7LvbVO6K9R0C6iyK7DkDhXVhE+aPnpyROFYcCICqmaGeSdfVt+j5PcNthKTlvuWcH+q3GRbeDWkkjvR7oAHcAKXm+8J45OSHwVnOfY6V35b5XfZxgCvj5TV0mXCVdx4QAAAF6wMqgzgAABAMASDBGAiEA0Vnv+kkPvcV6pwu0E2+LvJULhLW79+bS2Tc463fM6wYCIQD9Cz4htCd4bIIVFbKw59uCCSYP6gv/5/mCjWE+Hz0KLzANBgkqhkiG9w0BAQsFAAOCAQEAbTJP2tk3JYkea8a0GPueXbwkOqBWLgp9vElCWQaitEtVl42Wsdm5GPc8/lZGOojep/rUZTCwZ3hR3Xag42R8NpeFUboijlqSxvHlOJKTA68wIGWjGgNk7IA2IaL4awIea68XZAVeHqBH1D+05VNXnm7AyoXJPtVDVTO5k0uC8XBpRTf0ZrykzqISbSov0gsX9/trJA56VNN1cwlg9EZJkaC/0oYE6Rpddw90FCHItBGWurG/kTMFZnMk9Z6Lg1j1kwrarpppkk8qRGDh80JlZpxplCdi9rbK8DKGrMkb4m3bex6WTaowZhMwvjQWIrZJQAAzyN0kOFhPd3JQMfF5gw==`

	privateKey = ``
)

type request struct {
	XMLName xml.Name       `xml:"soapenv:Envelope"`
	Text    string         `xml:",chardata"`
	Soapenv string         `xml:"xmlns:soapenv,attr"`
	V1      string         `xml:"xmlns:v1,attr"`
	Header  *requestHeader `xml:"soapenv:Header"`
	Body    *requestBody   `xml:"soapenv:Body"`
}

type requestHeader struct {
	XMLName  xml.Name  `xml:"soapenv:Header"`
	Text     string    `xml:",chardata"`
	Security *security `xml:"wsse:Security"`
}

type security struct {
	Text                string               `xml:",chardata"`
	Wsse                string               `xml:"xmlns:wsse,attr"`
	Wsu                 string               `xml:"xmlns:wsu,attr"`
	BinarySecurityToken *binarySecurityToken `xml:"wsse:BinarySecurityToken"`
	UsernameToken       *usernameToken       `xml:"wsse:UsernameToken"`
	//Timestamp           *timestamp           `xml:"wsu:Timestamp"`
	Signature *signature `xml:"ds:Signature"`
}

type usernameToken struct {
	Text     string    `xml:",chardata"`
	ID       string    `xml:"wsu:Id,attr"`
	Username string    `xml:"wsse:Username"`
	Password *password `xml:"wsse:Password"`
	Nonce    *nonce    `xml:"wsse:Nonce"`
	Created  string    `xml:"wsu:Created"`
}

type password struct {
	Text string `xml:",chardata"`
	Type string `xml:"Type,attr"`
}

type nonce struct {
	Text         string `xml:",chardata"`
	EncodingType string `xml:"EncodingType,attr"`
}

type binarySecurityToken struct {
	Text         string `xml:",chardata"`
	EncodingType string `xml:"EncodingType,attr"`
	ValueType    string `xml:"ValueType,attr"`
	ID           string `xml:"wsu:Id,attr"`
}

/*type timestamp struct {
	Text    string `xml:",chardata"`
	ID      string `xml:"wsu:Id,attr"`
	Created string `xml:"wsu:Created"`
	Expires string `xml:"wsu:Expires"`
}*/

type signature struct {
	Text           string      `xml:",chardata"`
	ID             string      `xml:"Id,attr"`
	Ds             string      `xml:"xmlns:ds,attr"`
	SignedInfo     *signedInfo `xml:"ds:SignedInfo"`
	SignatureValue string      `xml:"ds:SignatureValue"`
	KeyInfo        *keyInfo    `xml:"ds:KeyInfo"`
}

type signedInfo struct {
	XMLName                xml.Name                `xml:"ds:SignedInfo"`
	Text                   string                  `xml:",chardata"`
	CanonicalizationMethod *canonicalizationMethod `xml:"ds:CanonicalizationMethod"`
	SignatureMethod        *signatureMethod        `xml:"ds:SignatureMethod"`
	DsReference            *dsReference            `xml:"ds:Reference"`
}

type canonicalizationMethod struct {
	Text                string               `xml:",chardata"`
	Algorithm           string               `xml:"Algorithm,attr"`
	InclusiveNamespaces *inclusiveNamespaces `xml:"ec:InclusiveNamespaces"`
}

type inclusiveNamespaces struct {
	Text       string `xml:",chardata"`
	PrefixList string `xml:"PrefixList,attr"`
	Ec         string `xml:"xmlns:ec,attr"`
}

type signatureMethod struct {
	Text      string `xml:",chardata"`
	Algorithm string `xml:"Algorithm,attr"`
}

type dsReference struct {
	Text         string        `xml:",chardata"`
	URI          string        `xml:"URI,attr"`
	Transforms   *transforms   `xml:"ds:Transforms"`
	DigestMethod *digestMethod `xml:"ds:DigestMethod"`
	DigestValue  string        `xml:"ds:DigestValue"`
}

type transforms struct {
	Text      string     `xml:",chardata"`
	Transform *transform `xml:"ds:Transform"`
}

type transform struct {
	Text                string               `xml:",chardata"`
	Algorithm           string               `xml:"Algorithm,attr"`
	InclusiveNamespaces *inclusiveNamespaces `xml:"ec:InclusiveNamespaces"`
}

type digestMethod struct {
	Text      string `xml:",chardata"`
	Algorithm string `xml:"Algorithm,attr"`
}

type keyInfo struct {
	Text                   string                  `xml:",chardata"`
	ID                     string                  `xml:"Id,attr"`
	SecurityTokenReference *securityTokenReference `xml:"wsse:SecurityTokenReference"`
}

type securityTokenReference struct {
	Text      string         `xml:",chardata"`
	ID        string         `xml:"wsu:Id,attr"`
	Reference *wsseReference `xml:"wsse:Reference"`
}

type wsseReference struct {
	Text      string `xml:",chardata"`
	URI       string `xml:"URI,attr"`
	ValueType string `xml:"ValueType,attr"`
}

type requestBody struct {
	XMLName   xml.Name   `xml:"soapenv:Body"`
	Text      string     `xml:",chardata"`
	ID        string     `xml:"wsu:Id,attr"`
	Wsu       string     `xml:"xmlns:wsu,attr"`
	SearchHC2 *searchHC2 `xml:"v1:consultarHC2"`
}

type searchHC2 struct {
	Text       string      `xml:",chardata"`
	Solicitude *solicitude `xml:"v1:solicitud"`
}

type solicitude struct {
	Text               string `xml:",chardata"`
	Key                string `xml:"v1:clave"`
	ID                 string `xml:"v1:identificacion"`
	FirstLastName      string `xml:"v1:primerApellido"`
	Product            string `xml:"v1:producto"`
	IdentificationType string `xml:"v1:tipoIdentificacion"`
	User               string `xml:"v1:usuario"`
}

// GenerateRandomData returns secure random data with the given size
func GenerateRandomData(bitSize int) []byte {
	buffer := make([]byte, bitSize)

	for i := 0; i < _attemptsToReadRandomData; i++ {
		_, err := rand.Read(buffer)
		if err == nil {
			break
		}
	}

	return buffer
}

// GenerateRandomHexString returns secure random hex string
func GenerateRandomHexString(bitSize int) string {
	return hex.EncodeToString(GenerateRandomData(bitSize))
}

func generateID(prefix string) string {
	return prefix + "-" + strings.ToUpper(GenerateRandomHexString(16))
}

func getSignatureValue(signInfo *signedInfo) (string, error) {
	signInfoBytes, err := xml.Marshal(signInfo)
	if err != nil {
		return "", err
	}

	h := sha1.New()
	_, err = h.Write(signInfoBytes)
	if err != nil {
		return "", err
	}

	pem09, _ := pem.Decode([]byte(privateKey))

	privateKey09, err := x509.ParsePKCS1PrivateKey(pem09.Bytes)
	if err != nil {
		return "", err
	}

	sign, err := rsa.SignPKCS1v15(rand.Reader, privateKey09, crypto.SHA1, h.Sum(nil))
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(sign), nil
}

func getDigestValue(body *requestBody) (string, error) {
	bodyBytes, err := xml.Marshal(body)
	if err != nil {
		return "", err
	}

	h := sha1.New()

	_, err = h.Write(bodyBytes)
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(h.Sum(nil)), nil
}

func doRequest(payload string) error {
	cert, err := tls.X509KeyPair([]byte(certPublic), []byte(privateKey))
	if err != nil {
		return err
	}

	header := http.Header{}
	header.Set("Content-Type", "text/xml;charset=UTF-8")
	header.Set("SOAPAction", "")
	header.Set("Accept-Encoding", "gzip,deflate")

	cli, err := client.New()
	if err != nil {
		return err
	}

	cli.HTTPClient.Transport = &http.Transport{
		TLSClientConfig: &tls.Config{
			Certificates: []tls.Certificate{
				cert,
			},
			InsecureSkipVerify: true,
		},
	}

	response, err := cli.SendRequest(context.Background(), http.MethodPost, "https://demo-servicesesb.datacredito.com.co/wss/dhws3/services/DHServicePlus", header, payload)
	if err != nil {
		return err
	}

	defer logger.New("test").CloseOrLog(context.Background(), response.Body)

	buf := new(bytes.Buffer)

	_, err = buf.ReadFrom(response.Body)
	if err != nil {
		return err
	}

	newStr := buf.String()
	fmt.Println(newStr)

	if response.StatusCode != http.StatusOK {
		return errors.New("http status different")
	}

	return nil
}

func main() {
	currentTime := time.Now()
	reqBody := &requestBody{
		XMLName: xml.Name{},
		Text:    "",
		ID:      generateID("id"),
		Wsu:     "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd",
		SearchHC2: &searchHC2{
			Text: "",
			Solicitude: &solicitude{
				Text:               "",
				Key:                "14ELJ",
				ID:                 "1144194216",
				FirstLastName:      "MORAN",
				Product:            "64",
				IdentificationType: "1",
				User:               "901189979",
			},
		},
	}

	digestValue, err := getDigestValue(reqBody)
	if err != nil {
		panic(err)
	}

	singInfo := &signedInfo{
		Text: "",
		CanonicalizationMethod: &canonicalizationMethod{
			Text:      "",
			Algorithm: "http://www.w3.org/2001/10/xml-exc-c14n#",
			InclusiveNamespaces: &inclusiveNamespaces{
				Text:       "",
				PrefixList: "soapenv v1",
				Ec:         "http://www.w3.org/2001/10/xml-exc-c14n#",
			},
		},
		SignatureMethod: &signatureMethod{
			Text:      "",
			Algorithm: "http://www.w3.org/2000/09/xmldsig#rsa-sha1",
		},
		DsReference: &dsReference{
			Text:        "",
			URI:         "#" + reqBody.ID,
			DigestValue: digestValue,
			DigestMethod: &digestMethod{
				Text:      "",
				Algorithm: "http://www.w3.org/2000/09/xmldsig#sha1",
			},
			Transforms: &transforms{
				Text: "",
				Transform: &transform{
					Text:      "",
					Algorithm: "http://www.w3.org/2001/10/xml-exc-c14n#",
					InclusiveNamespaces: &inclusiveNamespaces{
						Text:       "",
						PrefixList: "v1",
						Ec:         "http://www.w3.org/2001/10/xml-exc-c14n#",
					},
				},
			},
		},
	}

	signValue, err := getSignatureValue(singInfo)
	if err != nil {
		panic(err)
	}

	binarySecurity := &binarySecurityToken{
		Text:         certPublicClear,
		EncodingType: "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary",
		ValueType:    "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3",
		ID:           generateID("X509"),
	}

	reqHeader := &requestHeader{
		XMLName: xml.Name{},
		Security: &security{
			Text: "",
			Wsse: "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd",
			Wsu:  "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd",
			UsernameToken: &usernameToken{
				Text:     "",
				ID:       generateID("UsernameToken"),
				Username: "2-901189979",
				Created:  currentTime.Format(layoutTime),
				Password: &password{
					Type: "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText",
					Text: "TruoraContraElMundo02!",
				},
				Nonce: &nonce{
					EncodingType: "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary",
					Text:         base64.StdEncoding.EncodeToString([]byte("TruoraContraElMundo02!")),
				},
			},
			BinarySecurityToken: binarySecurity,
			/*Timestamp: &timestamp{
				Text:    "",
				ID:      generateID("TS"),
				Created: currentTime.Format(layoutTime),
				Expires: currentTime.Add(5 * time.Minute).Format(layoutTime),
			},*/
			Signature: &signature{
				Text:           "",
				ID:             generateID("SIG"),
				Ds:             "http://www.w3.org/2000/09/xmldsig#",
				SignatureValue: signValue,
				SignedInfo:     singInfo,
				KeyInfo: &keyInfo{
					Text: "",
					ID:   generateID("KI"),
					SecurityTokenReference: &securityTokenReference{
						Text: "",
						ID:   generateID("STR"),
						Reference: &wsseReference{
							Text:      "",
							URI:       "#" + binarySecurity.ID,
							ValueType: "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3",
						},
					},
				},
			},
		},
	}

	req := &request{
		XMLName: xml.Name{},
		Text:    "",
		Soapenv: "http://schemas.xmlsoap.org/soap/envelope/",
		V1:      "http://ws.hc2.dc.com/v1",
		Body:    reqBody,
		Header:  reqHeader,
	}

	xmlBytes, err := xml.Marshal(req)
	if err != nil {
		panic(err)
	}

	payload := string(xmlBytes)
	fmt.Println(payload)

	err = doRequest(payload)
	if err != nil {
		panic(err)
	}
}
